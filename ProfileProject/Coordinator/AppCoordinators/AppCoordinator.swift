//
//  AppCoordinator.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/6/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import UIKit

class AppCoordinator: RootCoordinator, Coordinator {
    
    fileprivate let navigationController: UINavigationController
    var childCoordinators = [Coordinator]()
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.configureNavigationBar()
    }
    
    func start() {
        showTimelineViewController()
    }
    
    func showTimelineViewController() {
        let profileCoordinator = ProfileCoordinator(navigationController: navigationController)
        profileCoordinator.rootCoordinator = self
        profileCoordinator.start()
        childCoordinators.append(profileCoordinator)
    }
    
    private func configureNavigationBar() {
        navigationController.setNavigationBarHidden(false, animated: false)
        navigationController.navigationBar.barTintColor = .statusBarColor
        navigationController.navigationBar.tintColor = .statusBarColor
        navigationController.navigationBar.backgroundColor = .statusBarColor
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.shadowImage = UIImage()
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController.navigationBar.prefersLargeTitles = true
        navigationController.navigationItem.largeTitleDisplayMode = .never
        navigationController.navigationBar.largeTitleTextAttributes = [
            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
    }
}
extension AppCoordinator: ProfileCoordinatorDelegate { }
