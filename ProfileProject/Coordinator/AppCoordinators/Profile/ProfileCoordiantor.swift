//
//  ProfileCoordiantor.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/6/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import UIKit

protocol ProfileCoordinatorDelegate: class {
    
}
class ProfileCoordinator: Coordinator, RootCoordinator {
    
    weak var rootCoordinator: ProfileCoordinatorDelegate?
    
    let rootNavigationController: UINavigationController
    var childCoordinators: [Coordinator] = [Coordinator]()
    fileprivate var profileViewController: ProfileViewController!
    fileprivate var profileAssembler: ProfileAssemblerDelegate
    
    init(navigationController: UINavigationController) {
        rootNavigationController = navigationController
        rootNavigationController.modalPresentationStyle = .fullScreen
        rootNavigationController.modalPresentationCapturesStatusBarAppearance = true
        profileAssembler = ProfileAssembler()
    }
    
    func start() {
        profileViewController = profileAssembler.resolve(with: self)
        rootNavigationController.setViewControllers([profileViewController], animated: false)
        profileViewController.navigationItem.title = "Profile"
    }
}
extension ProfileCoordinator: ProfileViewModelCoordinator {
    
}
