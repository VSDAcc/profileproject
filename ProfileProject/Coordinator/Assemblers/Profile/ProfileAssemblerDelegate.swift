//
//  ProfileAssemblerDelegate.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/6/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import Foundation

protocol ProfileAssemblerDelegate: ProfileInfoAssembler { }
class ProfileAssembler: ProfileAssemblerDelegate { }

protocol ProfileInfoAssembler {
    func resolve(with coordinator: ProfileViewModelCoordinator?) -> ProfileViewController
    func resolve(with coordinator: ProfileViewModelCoordinator?) -> ProfileViewModelInput
    func resolve() -> ProfileServicesStrategy
}
extension ProfileInfoAssembler {
    func resolve(with coordinator: ProfileViewModelCoordinator?) -> ProfileViewController {
        return ProfileViewController(viewModel: resolve(with: coordinator))
    }
    
    func resolve(with coordinator: ProfileViewModelCoordinator?) -> ProfileViewModelInput {
        return ProfileViewModel(coordinator, resolve())
    }
    
    func resolve() -> ProfileServicesStrategy {
        return ProfileService(service: ProfileProvider())
    }
}
