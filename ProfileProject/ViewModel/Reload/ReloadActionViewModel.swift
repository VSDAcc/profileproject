//
//  ReloadActionViewModel.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/8/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import Foundation

protocol ReloadActionViewModelDelegate: class {
    func viewModelReloadActionDidPressed(_ viewModel: ReloadActionViewModelInput)
}
protocol ReloadActionViewModelInput: class {
    var delegate: ReloadActionViewModelDelegate? { get set }
    var actionTitle: String { get set }
    func reload()
}
class ReloadActionViewModel: ReloadActionViewModelInput {
    
    weak var delegate: ReloadActionViewModelDelegate?
    var actionTitle: String
    
    init(actionTitle: String) {
        self.actionTitle = actionTitle
    }
    
    func reload() {
        delegate?.viewModelReloadActionDidPressed(self)
    }
}
