//
//  ProfileViewModel.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/6/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import Foundation
import UIKit

protocol ProfileViewModelCoordinator: class {
    
}
protocol ProfileViewModelOutput: class {
    func viewModelDidLoadData(_ viewModel: ProfileViewModelInput)
    func viewModelWillLoadData(_ viewModel: ProfileViewModelInput)
    func viewModel(_ viewModel: ProfileViewModelInput, didHandleError error: String)
    func viewModel(_ viewModel: ProfileViewModelInput, updateHeaderWith profile: Profile)
    func viewModelNeedsUpdateActionView(_ viewModel: ReloadActionViewModelInput)
}
protocol ProfileViewModelInput: ViewModelCellPresentable, ReloadActionViewModelDelegate {
    var view: ProfileViewModelOutput? { get set }
    var coordinator: ProfileViewModelCoordinator? { get set }
    func createProfileNameAttributedText(title: String, firstName: String, lastName: String, userName: String) -> NSMutableAttributedString
    func getProfile()
    func loadData()
}
class ProfileViewModel: ProfileViewModelInput {
    
    fileprivate var sections = [SectionRowsRepresentable]()
    fileprivate var profileInfoSection = ProfileInformationSectionModel()
    private let errorMessage = "Failed to load"
    private var reloadActionViewModel: ReloadActionViewModelInput
    
    weak var view: ProfileViewModelOutput?
    weak var coordinator: ProfileViewModelCoordinator?
    
    private let profileService: ProfileServicesStrategy
    
    init(_ coordinator: ProfileViewModelCoordinator? = nil,
         _ profileService: ProfileServicesStrategy = ProfileService(service: ProfileProvider())) {
        self.coordinator = coordinator
        self.profileService = profileService
        self.reloadActionViewModel = ReloadActionViewModel(actionTitle: errorMessage)
        self.reloadActionViewModel.delegate = self
    }
    
    func loadData() {
        view?.viewModelNeedsUpdateActionView(reloadActionViewModel)
        sections = [profileInfoSection]
    }
    
    func getProfile() {
        view?.viewModelWillLoadData(self)
        profileService.getProfile(with: .profile, onSuccess: { [weak self] (profile) in
            guard let strongSelf = self else { return }
            strongSelf.profileInfoSection.create(with: profile)
            strongSelf.view?.viewModelDidLoadData(strongSelf)
            strongSelf.view?.viewModel(strongSelf, updateHeaderWith: profile)
        }) { [weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.view?.viewModel(strongSelf, didHandleError: error)
        }
    }
    
    func createProfileNameAttributedText(title: String, firstName: String, lastName: String, userName: String) -> NSMutableAttributedString {
        let title = "\(title.capitalizingFirstLetter())."
        let firstName = "\(firstName.capitalizingFirstLetter())"
        let lastName = "\(lastName.capitalizingFirstLetter())"
        let userName = "\(userName)"
        
        var titleFont = UIFont.systemFont(ofSize: 24, weight: .medium)
        var lastNameFont = UIFont.systemFont(ofSize: 24, weight: .bold)
        var userNameFont = UIFont.systemFont(ofSize: 24, weight: .bold)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            titleFont = UIFont.systemFont(ofSize: 34, weight: .medium)
            lastNameFont = UIFont.systemFont(ofSize: 34, weight: .bold)
            userNameFont = UIFont.systemFont(ofSize: 26, weight: .regular)
        }
        
        let attributedText = NSMutableAttributedString(string: title, attributes: [
            .font: titleFont,
            .strokeColor: UIColor.black,
            .foregroundColor: UIColor.black
            ])
        attributedText.append(NSAttributedString(string: " \(firstName)", attributes: [
            .font: titleFont,
            .strokeColor: UIColor.black,
            .foregroundColor: UIColor.black
            ]))
        attributedText.append(NSAttributedString(string: " \(lastName)", attributes: [
            .font: lastNameFont,
            .strokeColor: UIColor.black,
            .foregroundColor: UIColor.black
            ]))
        attributedText.append(NSAttributedString(string: "\n\(userName)", attributes: [
            .font: userNameFont,
            .strokeColor: UIColor.loadingGrayColor,
            .foregroundColor: UIColor.loadingGrayColor
            ]))
        let lenght = attributedText.string.count
        let paragpraphStyle = NSMutableParagraphStyle()
        paragpraphStyle.alignment = .center
        paragpraphStyle.paragraphSpacing = 1.0
        paragpraphStyle.lineSpacing = 1.0
        attributedText.addAttribute(.paragraphStyle, value: paragpraphStyle, range: NSRange(location: 0, length: lenght))
        return attributedText
    }
}
extension ProfileViewModel {
    func viewModelReloadActionDidPressed(_ viewModel: ReloadActionViewModelInput) {
        getProfile()
    }
}
extension ProfileViewModel {
    
    func numberOfSections() -> Int {
        return sections.count
    }
    
    func selectedItemAt(section: Int, row: Int) -> CellIdentifiable {
        return sections[section].rows[row]
    }
    
    func numberOfItemsInSection(section: Int) -> Int {
        return sections[section].rows.count
    }
}

