//
//  APIProfile.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/7/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import Foundation
import Moya

enum APIProfile {
    case profile
}
extension APIProfile: ProfileTargetType {
    
    var path: String {
        switch self {
        case .profile: return ""
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .profile:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case .profile:
            return Task.requestParameters(parameters: {
                var parameters = [String : Any]()
                parameters["login"] = Configuration.login
                parameters["password"] = Configuration.password
                return parameters
            }(), encoding: JSONEncoding.prettyPrinted)
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .profile:
            return ["Content-type" : "application/json"]
        }
    }
}
