//
//  APITarget.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/21/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation
import Moya

protocol ProfileTargetType: TargetType { }
extension ProfileTargetType {
    
    var baseURL: URL {
        return Configuration.profileEndpoint
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var headers: [String : String]? {
        return ["accept" : "application/json"]
    }
}
