//
//  ProfileServices.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/7/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import Foundation

protocol ProfileServicesStrategy {
    func getProfile(with target: APIProfile,
                    onSuccess: @escaping (_ model: Profile) -> (),
                    onFailure: @escaping (_ error: String) -> ())
}
class ProfileService: ProfileServicesStrategy {
    
    public var service: ProfileServicesStrategy
    
    init(service: ProfileServicesStrategy) {
        self.service = service
    }
    
    func getProfile(with target: APIProfile,
                    onSuccess: @escaping (_ model: Profile) -> (),
                    onFailure: @escaping (_ error: String) -> ()){
        self.service.getProfile(with: target, onSuccess: onSuccess, onFailure: onFailure)
    }
}
