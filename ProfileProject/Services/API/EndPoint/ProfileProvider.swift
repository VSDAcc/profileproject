//
//  ProfileProvider.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/7/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import Foundation

class ProfileProvider: ProfileServicesStrategy, Handler {
    
    private let provider = APIProvider<APIProfile>()
    
    public func getProfile(with target: APIProfile,
                    onSuccess: @escaping (Profile) -> (),
                    onFailure: @escaping (String) -> ()) {
        self.provider.request(target) { (result) in
            self.handle(result: result, onSuccess: { (response) in
                let profileArray = try? JSONDecoder().decode(ProfileResult.self, from: response.data)
                if let profile = profileArray?.profiles.first {
                    onSuccess(profile)
                } else {
                    onFailure("Ooops something went wrong")
                }
            }, onError: { (error) in
                onFailure(error.localizedDescription)
            })
        }
    }
}
