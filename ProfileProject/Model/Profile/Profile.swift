//
//  Profile.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/7/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import Foundation

struct ProfileResult {
    let profiles: [Profile]
}
extension ProfileResult: Codable {
    
    enum CodingKeys: String, CodingKey {
        case results = "results"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let profiles: [Profile] = try container.decodeIfPresent([Profile].self, forKey: .results) ?? []
        
        self.init(profiles: profiles)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(profiles, forKey: .results)
    }
}
struct Profile {
    var firstName: String
    var lastName: String
    var title: String
    var cell: String
    var nation: String
    var phone: String
    var email: String
    var gender: String
    var age: Int
    var birthday: String
    var picture: Picture
    var location: Location
    var registration: ProfileRegistration
    var login: ProfileLogin
}
extension Profile: Codable {
    
    enum CodingKeys: String, CodingKey {
        case email = "email"
        case gender = "gender"
        case cell = "cell"
        case nation = "nat"
        case phone = "phone"
        case dob = "dob"
        case name = "name"
        case location = "location"
        case picture = "picture"
        case registered = "registered"
        case login = "login"
    }
    
    enum DateOfBirthCodingKeys: String, CodingKey {
        case age = "age"
        case birthday = "date"
    }
    
    enum NameCodingKeys: String, CodingKey {
        case firstName = "first"
        case lastName = "last"
        case title = "title"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let email = try container.decodeIfPresent(String.self, forKey: .email) ?? "empty"
        let gender = try container.decodeIfPresent(String.self, forKey: .gender) ?? "empty"
        let nation = try container.decodeIfPresent(String.self, forKey: .nation) ?? "empty"
        let phone = try container.decodeIfPresent(String.self, forKey: .phone) ?? "empty"
        let cell = try container.decodeIfPresent(String.self, forKey: .cell) ?? "empty"
        
        let nameContainer = try container.nestedContainer(keyedBy: NameCodingKeys.self, forKey: .name)
        let firstName = try nameContainer.decodeIfPresent(String.self, forKey: .firstName) ?? "empty"
        let lastName = try nameContainer.decodeIfPresent(String.self, forKey: .lastName) ?? "empty"
        let title = try nameContainer.decodeIfPresent(String.self, forKey: .title) ?? "empty"
        
        let dateContainer = try container.nestedContainer(keyedBy: DateOfBirthCodingKeys.self, forKey: .dob)
        let age = try dateContainer.decodeIfPresent(Int.self, forKey: .age) ?? 0
        let birthday = try dateContainer.decodeIfPresent(String.self, forKey: .birthday) ?? "empty"
        
        let defaultPicture = Picture(large: "", medium: "", thumbnail: "")
        let defaultLocation = Location(city: "empty", latitude: "0.0", longitude: "0.0", state: "", postcode: 0, street: "", timezone: "", timeOffset: "")
        let defaultProfileRegistration = ProfileRegistration(age: 0, registrationDate: "")
        let defaultProfileLogin = ProfileLogin(username: "")
        
        let picture: Picture = try container.decodeIfPresent(Picture.self, forKey: .picture) ?? defaultPicture
        let location: Location = try container.decodeIfPresent(Location.self, forKey: .location) ?? defaultLocation
        let profileRegistration: ProfileRegistration = try container.decodeIfPresent(ProfileRegistration.self, forKey: .registered) ?? defaultProfileRegistration
        let login: ProfileLogin = try container.decodeIfPresent(ProfileLogin.self, forKey: .login) ?? defaultProfileLogin
        
        self.init(firstName: firstName, lastName: lastName, title: title, cell: cell, nation: nation, phone: phone, email: email, gender: gender, age: age, birthday: birthday, picture: picture, location: location, registration: profileRegistration, login: login)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encodeIfPresent(email, forKey: .email)
        try container.encodeIfPresent(gender, forKey: .gender)
        try container.encodeIfPresent(nation, forKey: .nation)
        try container.encodeIfPresent(phone, forKey: .phone)
        try container.encodeIfPresent(picture, forKey: .picture)
        try container.encodeIfPresent(location, forKey: .location)
        try container.encodeIfPresent(registration, forKey: .registered)
        
        var nameContainer = container.nestedContainer(keyedBy: NameCodingKeys.self, forKey: .name)
        try nameContainer.encodeIfPresent(firstName, forKey: .firstName)
        try nameContainer.encodeIfPresent(lastName, forKey: .lastName)
        try nameContainer.encodeIfPresent(title, forKey: .title)
        
        var dateContainer = container.nestedContainer(keyedBy: DateOfBirthCodingKeys.self, forKey: .dob)
        try dateContainer.encodeIfPresent(age, forKey: .age)
        try dateContainer.encodeIfPresent(birthday, forKey: .birthday)
    }
}
struct ProfileRegistration {
    var age: Int
    var registrationDate: String
}
extension ProfileRegistration: Codable {
    
    enum CodingKeys: String, CodingKey {
        case age = "age"
        case date = "date"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let age = try container.decodeIfPresent(Int.self, forKey: .age) ?? 0
        let date = try container.decodeIfPresent(String.self, forKey: .date) ?? "empty"
        self.init(age: age, registrationDate: date)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(age, forKey: .age)
        try container.encodeIfPresent(registrationDate, forKey: .date)
    }
}
struct ProfileLogin {
    var username: String
}
extension ProfileLogin: Codable {
    
    enum CodingKeys: String, CodingKey {
        case username = "username"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let username = try container.decodeIfPresent(String.self, forKey: .username) ?? "empty"
        self.init(username: username)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(username, forKey: .username)
    }
}
struct Picture {
    var large: String
    var medium: String
    var thumbnail: String
}
extension Picture: Codable {
    
    enum CodingKeys: String, CodingKey {
        case large = "large"
        case medium = "medium"
        case thumbnail = "thumbnail"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let large = try container.decodeIfPresent(String.self, forKey: .large) ?? "empty"
        let medium = try container.decodeIfPresent(String.self, forKey: .medium) ?? "empty"
        let thumbnail = try container.decodeIfPresent(String.self, forKey: .thumbnail) ?? "empty"
        self.init(large: large, medium: medium, thumbnail: thumbnail)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(large, forKey: .large)
        try container.encodeIfPresent(medium, forKey: .medium)
        try container.encodeIfPresent(thumbnail, forKey: .thumbnail)
    }
}
struct Location {
    var city: String
    var latitude: String
    var longitude: String
    var state: String
    var postcode: Int
    var street: String
    var timezone: String
    var timeOffset: String
}
extension Location: Codable {
    
    enum CodingKeys: String, CodingKey {
        case city = "city"
        case coordinates = "coordinates"
        case state = "state"
        case street = "street"
        case timezone = "timezone"
        case postcode = "postcode"
    }
    
    enum CodingKeysCoordinate: String, CodingKey {
        case latitude = "latitude";
        case longitude = "longitude"
    }
    
    enum CodingKeysTimezone: String, CodingKey {
        case description = "description";
        case offset = "offset"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let city = try container.decodeIfPresent(String.self, forKey: .city) ?? "empty"
        let state = try container.decodeIfPresent(String.self, forKey: .state) ?? "empty"
        let street = try container.decodeIfPresent(String.self, forKey: .street) ?? "empty"
        let postcode = try? container.decodeIfPresent(Int.self, forKey: .postcode) ?? 0
        
        let coordinateContainer = try container.nestedContainer(keyedBy: CodingKeysCoordinate.self, forKey: .coordinates)
        let lat = try coordinateContainer.decodeIfPresent(String.self, forKey: .latitude) ?? "0.0"
        let lng = try coordinateContainer.decodeIfPresent(String.self, forKey: .longitude) ?? "0.0"
        
        let timezoneContainer = try container.nestedContainer(keyedBy: CodingKeysTimezone.self, forKey: .timezone)
        let timezone = try timezoneContainer.decodeIfPresent(String.self, forKey: .description) ?? "empty"
        let timeOffset = try timezoneContainer.decodeIfPresent(String.self, forKey: .offset) ?? "empty"
        
        self.init(city: city, latitude: lat, longitude: lng, state: state, postcode: postcode ?? 0, street: street, timezone: timezone, timeOffset: timeOffset)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(city, forKey: .city)
        try container.encodeIfPresent(state, forKey: .state)
        try container.encodeIfPresent(street, forKey: .street)
        
        var timezoneContainer = encoder.container(keyedBy: CodingKeysTimezone.self)
        try timezoneContainer.encodeIfPresent(timezone, forKey: .description)
        try timezoneContainer.encodeIfPresent(timeOffset, forKey: .offset)
        
        var coordinateContainer = encoder.container(keyedBy: CodingKeysCoordinate.self)
        try coordinateContainer.encodeIfPresent(longitude, forKey: .longitude)
        try coordinateContainer.encodeIfPresent(latitude, forKey: .latitude)
    }
}

