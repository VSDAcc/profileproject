//
//  ProfileSectionModel.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/7/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import Foundation

class ProfileInformationSectionModel: BaseSectionModel {
    
    func create(with profile: Profile) {
        let adressModel = ProfilePostalAdressCellModel(profile: profile)
        let emailModel = ProfileEmailCellModel(profile: profile)
        let cellModel = ProfileCellCodeCellModel(profile: profile)
        let registeredModel = ProfileRegisteredCellModel(profile: profile)
        rows = [adressModel, emailModel, cellModel, registeredModel]
    }
    
    func clear() {
        rows = []
    }
}
