//
//  ProfileRegisteredCellModel.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/7/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import Foundation

class ProfileRegisteredCellModel: BaseCellModel {
    
    override var cellIdentifier: String {
        return ProfileRegisteredTableViewCell.reuseIdentifier
    }
    
    var date: String
    var title: String
    
    init(profile: Profile) {
        title = "Registered"
        date = profile.registration.registrationDate
    }
}
