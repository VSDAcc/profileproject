//
//  ProfileEmailCellModel.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/7/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import Foundation

class ProfileEmailCellModel: BaseCellModel {
    
    override var cellIdentifier: String {
        return ProfileEmailTableViewCell.reuseIdentifier
    }
    
    var email: String
    var title: String
    
    init(profile: Profile) {
        title = "E-mail"
        email = profile.email
    }
}
