//
//  ProfileCellCodeCellModel.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/7/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import Foundation

class ProfileCellCodeCellModel: BaseCellModel {
    
    override var cellIdentifier: String {
        return ProfileCellTableViewCell.reuseIdentifier
    }
    
    var cell: String
    var title: String
    
    init(profile: Profile) {
        title = "Cell"
        cell = profile.cell
    }
}
