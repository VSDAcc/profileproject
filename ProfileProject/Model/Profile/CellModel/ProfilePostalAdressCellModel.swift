//
//  ProfileCellModel.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/7/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import Foundation
import UIKit

struct HeightResultVisitor: ModelVisitor {
    
    let width: CGFloat
    
    init(width: CGFloat) {
        self.width = width
    }
    
    func visit(model: ProfilePostalAdressCellModel) -> CGFloat {
        return model.createAddressAttributedText().height(containerWidth: width)
    }
}
class ProfilePostalAdressCellModel: BaseCellModel, VisitableModel {
    
    override var cellIdentifier: String {
        return ProfilePostalAdressTableViewCell.reuseIdentifier
    }
    
    var city: String
    var street: String
    var state: String
    var postcode: Int
    var title: String
    
    init(profile: Profile) {
        title = "Postal Adress"
        city = profile.location.city
        street = profile.location.street
        state = profile.location.state
        postcode = profile.location.postcode
    }
    
    func accept<V>(visitor: V) -> V.T where V : ModelVisitor {
        return visitor.visit(model: self)
    }
    
    func createAddressAttributedText() -> NSMutableAttributedString {
        let postCode = "\(self.postcode)"
        let street = "\(self.street.capitalizingFirstLetter())"
        let state = "\(self.state.capitalizingFirstLetter())"
        let city = "\(self.city.capitalizingFirstLetter())"
        
        var textFont = UIFont.systemFont(ofSize: 16, weight: .medium)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            textFont = UIFont.systemFont(ofSize: 26, weight: .medium)
        }
        
        let attributedText = NSMutableAttributedString(string: postCode, attributes: [
            .font: textFont,
            .strokeColor: UIColor.black,
            .foregroundColor: UIColor.black
            ])
        attributedText.append(NSAttributedString(string: " \(state)", attributes: [
            .font: textFont,
            .strokeColor: UIColor.black,
            .foregroundColor: UIColor.black
            ]))
        attributedText.append(NSAttributedString(string: "\n\(city)", attributes: [
            .font: textFont,
            .strokeColor: UIColor.black,
            .foregroundColor: UIColor.black
            ]))
        attributedText.append(NSAttributedString(string: "\n\(street)", attributes: [
            .font: textFont,
            .strokeColor: UIColor.black,
            .foregroundColor: UIColor.black
            ]))
        let lenght = attributedText.string.count
        let paragpraphStyle = NSMutableParagraphStyle()
        paragpraphStyle.alignment = .right
        paragpraphStyle.paragraphSpacing = 1.0
        paragpraphStyle.lineSpacing = 1.0
        attributedText.addAttribute(.paragraphStyle, value: paragpraphStyle, range: NSRange(location: 0, length: lenght))
        return attributedText
    }
}
