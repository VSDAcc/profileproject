//
//  ProfileViewController.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/6/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import UIKit

protocol ProfileViewControllerInput: ProfileViewModelOutput, PresenterAlertHandler { }

class ProfileViewController: UIViewController, ProfileViewControllerInput {
    
    fileprivate lazy var tableView: UITableView = self.createTableView()
    fileprivate lazy var headerView: ProfileHeaderView = self.createProfileHeaderView()
    fileprivate lazy var progressHUD: ProgressHudView = self.createProgressHudView()
    fileprivate lazy var reloadView: ReloadActionView = self.createReloadActionView()
    fileprivate var defaultHeaderViewHeight: CGFloat = 200.0
    fileprivate var defaultHeightForCell: CGFloat = 50.0
    fileprivate let viewModel: ProfileViewModelInput
    
    //MARK:-Loading
    init(viewModel: ProfileViewModelInput) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.view = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadData()
        viewModel.getProfile()
        setupAllConstraintsToView()
        configureTableView()
        view.backgroundColor = .white
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        DispatchQueue.main.async {
            self.tableView.tableHeaderView?.layoutIfNeeded()
            self.tableView.tableHeaderView = self.tableView.tableHeaderView
        }
    }
    //MARK:-SetupViews
    private func configureTableView() {
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.tableHeaderView?.layoutIfNeeded()
        tableView.tableHeaderView = headerView
        tableView.estimatedRowHeight = defaultHeightForCell
        tableView.rowHeight = UITableView.automaticDimension
        tableView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 10.0, right: 0)
        tableView.register(ProfilePostalAdressTableViewCell.self, forCellReuseIdentifier: ProfilePostalAdressTableViewCell.reuseIdentifier)
        tableView.register(ProfileEmailTableViewCell.self, forCellReuseIdentifier: ProfileEmailTableViewCell.reuseIdentifier)
        tableView.register(ProfileCellTableViewCell.self, forCellReuseIdentifier: ProfileCellTableViewCell.reuseIdentifier)
        tableView.register(ProfileRegisteredTableViewCell.self, forCellReuseIdentifier: ProfileRegisteredTableViewCell.reuseIdentifier)
    }
    
    private func createProgressHudView() -> ProgressHudView {
        let hud = ProgressHudView(text: "Loading...", indicatorStyle: .white, indicatorColor: .loadingGrayColor, textColor: .loadingGrayColor, blurEffectStyle: .light)
        hud.translatesAutoresizingMaskIntoConstraints = false
        hud.hide()
        view.addSubview(hud)
        view.bringSubviewToFront(hud)
        return hud
    }
    
    private func createTableView() -> UITableView {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        return tableView
    }
    
    private func createProfileHeaderView() -> ProfileHeaderView {
        let headerView = ProfileHeaderView()
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.backgroundColor = .clear
        tableView.addSubview(headerView)
        return headerView
    }
    
    private func createReloadActionView() -> ReloadActionView {
        let reloadView = ReloadActionView()
        reloadView.translatesAutoresizingMaskIntoConstraints = false
        reloadView.backgroundColor = .clear
        reloadView.isHidden = true
        view.addSubview(reloadView)
        return reloadView
    }
    //MARK:-SetupConstraints
    fileprivate var headerViewHeightConstraint: NSLayoutConstraint?
    
    private func setupAllConstraintsToView() {
        setupConstraintsToTableView()
        setupConstraintsHeaderView()
        setupConstraintsToProgressHUDView()
        setupConstraintsToReloadView()
    }
    
    private func setupConstraintsToProgressHUDView() {
        progressHUD.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        progressHUD.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -60).isActive = true
        progressHUD.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1/3).isActive = true
        progressHUD.heightAnchor.constraint(equalToConstant: 55.0).isActive = true
    }
    
    private func setupConstraintsToTableView() {
        tableView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    private func setupConstraintsHeaderView() {
        headerView.topAnchor.constraint(equalTo: tableView.topAnchor).isActive = true
        headerView.centerXAnchor.constraint(equalTo: tableView.centerXAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: tableView.widthAnchor).isActive = true
    }
    
    private func setupConstraintsToReloadView() {
        reloadView.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor).isActive = true
        reloadView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        reloadView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        reloadView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}
extension ProfileViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItemsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let profileModel = viewModel.selectedItemAt(section: indexPath.section, row: indexPath.row)
        let profileCell = tableView.dequeueReusableCell(withIdentifier: profileModel.cellIdentifier, for: indexPath) as! IdentifiableTableViewCell
        profileCell.updateModel(profileModel, viewModel: viewModel)
        return profileCell
    }
}
extension ProfileViewController {
    
    func viewModelDidLoadData(_ viewModel: ProfileViewModelInput) {
        DispatchQueue.main.async {
            self.progressHUD.hide()
            self.tableView.reloadData()
        }
    }
    
    func viewModel(_ viewModel: ProfileViewModelInput, updateHeaderWith profile: Profile) {
        DispatchQueue.main.async {
            self.headerView.update(profile, viewModel: viewModel)
        }
    }
    
    func viewModelWillLoadData(_ viewModel: ProfileViewModelInput) {
        DispatchQueue.main.async {
            self.reloadView.isHidden = true
            self.progressHUD.show()
        }
    }
    
    func viewModel(_ viewModel: ProfileViewModelInput, didHandleError error: String) {
        DispatchQueue.main.async {
            self.progressHUD.hide()
            self.presentAlertWith(title: "Error", massage: error)
            self.reloadView.isHidden = false
        }
    }
    
    func viewModelNeedsUpdateActionView(_ viewModel: ReloadActionViewModelInput) {
        DispatchQueue.main.async {
            self.reloadView.update(with: viewModel)
        }
    }
}
extension ProfileViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let animation = AnimationCellFactory.makeSlideIn(duration: 0.3, delayFactor: 0.2)
        let animator = CellAnimator(animation: animation)
        animator.animate(cell: cell, at: indexPath, in: tableView)
    }
}
