//
//  ProfileHeaderView.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/7/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import UIKit

class ProfileHeaderView: UIView {

    lazy var profileImageView: UIImageView = self.createProfileImageView()
    lazy var nameLabel: UILabel = self.createNameLabel()
    
    //MARK:-Loading
    init() {
        super.init(frame: UIScreen.main.bounds)
        setupAllConstraintsToViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        profileImageView.layer.cornerRadius = profileImageView.bounds.height / 2
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        NSLayoutConstraint.deactivate(landscapeLayoutConstraints + portraitLayoutConstraints)
        if UIApplication.shared.statusBarOrientation.isPortrait {
            NSLayoutConstraint.activate(portraitLayoutConstraints)
        } else {
            NSLayoutConstraint.activate(landscapeLayoutConstraints)
        }
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.layoutIfNeeded()
        }, completion: { (finished) in })
    }
    
    func update(_ model: Profile, viewModel: ProfileViewModelInput) {
        self.profileImageView.downloadImageUsingCache(stringURL: model.picture.large)
        let attributedText = viewModel.createProfileNameAttributedText(title: model.title, firstName: model.firstName, lastName: model.lastName.localized, userName: model.login.username)
        self.nameLabel.attributedText = attributedText
    }
    //MARK:-SetupViews
    private func createProfileImageView() -> UIImageView {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .white
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        addSubview(imageView)
        return imageView
    }
    
    private func createNameLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.lineBreakMode = .byWordWrapping
        label.backgroundColor = .clear
        label.numberOfLines = 0
        label.sizeToFit()
        label.adjustsFontForContentSizeCategory = true
        addSubview(label)
        return label
    }
    //MARK:-SetupConstraints
    fileprivate let itemsOffset: CGFloat = 8.0
    fileprivate var portraitLayoutConstraints: [NSLayoutConstraint] = []
    fileprivate var landscapeLayoutConstraints: [NSLayoutConstraint] = []
    
    private func setupAllConstraintsToViews() {
        setupPortraitConstraintsToProfileImageView()
        setupLandscapeConstraintsToProfileImageView()
        setupPortraitConstraintsToProfileNameLabel()
        setupLandscapeConstraintsToProfileNameLabel()
        NSLayoutConstraint.activate(portraitLayoutConstraints)
    }
    
    private func setupPortraitConstraintsToProfileImageView() {
        portraitLayoutConstraints.append(profileImageView.topAnchor.constraint(greaterThanOrEqualTo: topAnchor, constant: itemsOffset * 4))
        portraitLayoutConstraints.append(profileImageView.centerXAnchor.constraint(equalTo: centerXAnchor))
        portraitLayoutConstraints.append(profileImageView.widthAnchor.constraint(equalTo: profileImageView.heightAnchor))
        portraitLayoutConstraints.append(profileImageView.heightAnchor.constraint(greaterThanOrEqualToConstant: 150.0))
        portraitLayoutConstraints.append(profileImageView.bottomAnchor.constraint(equalTo: nameLabel.topAnchor, constant: -itemsOffset))
    }
    
    private func setupLandscapeConstraintsToProfileImageView() {
        landscapeLayoutConstraints.append(profileImageView.leftAnchor.constraint(greaterThanOrEqualTo: leftAnchor, constant: 22.0))
        landscapeLayoutConstraints.append(profileImageView.topAnchor.constraint(greaterThanOrEqualTo: topAnchor, constant: itemsOffset))
        landscapeLayoutConstraints.append(profileImageView.centerYAnchor.constraint(equalTo: centerYAnchor))
        landscapeLayoutConstraints.append(profileImageView.centerXAnchor.constraint(equalTo: centerXAnchor,  constant: -(bounds.midX)))
        landscapeLayoutConstraints.append(profileImageView.widthAnchor.constraint(lessThanOrEqualTo: profileImageView.heightAnchor))
        landscapeLayoutConstraints.append(profileImageView.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: -itemsOffset))
    }
    
    private func setupPortraitConstraintsToProfileNameLabel() {
        portraitLayoutConstraints.append(nameLabel.centerXAnchor.constraint(equalTo: centerXAnchor))
        portraitLayoutConstraints.append(nameLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.95))
        portraitLayoutConstraints.append(nameLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -22))
    }
    
    private func setupLandscapeConstraintsToProfileNameLabel() {
        landscapeLayoutConstraints.append(nameLabel.leftAnchor.constraint(equalTo: centerXAnchor, constant: 5))
        landscapeLayoutConstraints.append(nameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -20))
        landscapeLayoutConstraints.append(nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 22))
        landscapeLayoutConstraints.append(nameLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -22))
    }
}
