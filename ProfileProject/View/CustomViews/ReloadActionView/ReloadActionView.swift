//
//  ProfileBackgroundView.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/8/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import UIKit

class ReloadActionView: UIView {
    
    lazy var titleLabel: UILabel = self.createTitleLabel()
    lazy var reloadButton: UIButton = self.createReloadButton()
    
    var viewModel: ReloadActionViewModelInput?
    
    //MARK:-Loading
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .aliceBlue
        setupAllConstraintsToViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func update(with viewModel: ReloadActionViewModelInput) {
        self.viewModel = viewModel
        self.titleLabel.text = viewModel.actionTitle
    }
    //MARK:-Actions
    @objc func reloadButtonDidPressed(_ sender: UIButton) {
       viewModel?.reload()
    }
    //MARK:-SetupViews
    private func createReloadButton() -> UIButton {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        button.contentHorizontalAlignment = .center
        button.setTitle("Tap to retry", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = .loadingGrayColor
        button.layer.cornerRadius = 8.0
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(reloadButtonDidPressed(_:)), for: .touchUpInside)
        addSubview(button)
        return button
    }
    
    private func createTitleLabel() -> UILabel {
        let label = UILabel()
        label.textColor = UIColor.loadingGrayColor
        label.font = UIFont.systemFont(ofSize: 21, weight: .regular)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 1
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.sizeToFit()
        addSubview(label)
        return label
    }
    //MARK:-SetupConstraints
    private func setupAllConstraintsToViews() {
        setupConstraintsToReloadButton()
        setupConstraintsToTitleLabel()
    }
    
    private func setupConstraintsToTitleLabel() {
        titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        titleLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.8).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: reloadButton.topAnchor, constant: -20).isActive = true
    }
    
    private func setupConstraintsToReloadButton() {
        reloadButton.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        reloadButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        reloadButton.heightAnchor.constraint(equalToConstant: 30.0).isActive = true
        reloadButton.widthAnchor.constraint(equalToConstant: 105.0).isActive = true
    }
}
