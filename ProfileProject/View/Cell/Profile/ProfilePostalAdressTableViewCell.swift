//
//  ProfilePostalAdressTableViewCell.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/7/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import UIKit

class ProfilePostalAdressTableViewCell: IdentifiableTableViewCell {
    
    lazy var adressTitleLabel: UILabel = self.createLabel(with: UIFont.systemFont(ofSize: 16, weight: .regular), alignment: .left)
    lazy var adressDescriptionLabel: UILabel = self.createLabel(with: UIFont.systemFont(ofSize: 16, weight: .medium), alignment: .right)
    fileprivate lazy var separatingView: UIView = self.createSeparatingLineView()
    
    //MARK:-Loading
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupAllConstraintsToViews()
        selectionStyle = .none
        adressTitleLabel.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        adressDescriptionLabel.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        if UIDevice.current.userInterfaceIdiom == .pad {
            adressTitleLabel.font = UIFont.systemFont(ofSize: 26, weight: .regular)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateModel(_ model: CellIdentifiable?, viewModel: ViewModelCellPresentable?) {
        guard let _ = viewModel as? ProfileViewModel else { return }
        guard let model = model as? ProfilePostalAdressCellModel else { return }
        
        self.adressTitleLabel.text = model.title
        self.adressDescriptionLabel.attributedText = model.createAddressAttributedText()
        
        do {
            setNeedsLayout()
            layoutIfNeeded()
        }
    }
    //MARK:-SetupViews
    private func createLabel(with font: UIFont, alignment: NSTextAlignment) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = alignment
        label.lineBreakMode = .byWordWrapping
        label.font = font
        label.numberOfLines = 0
        label.sizeToFit()
        contentView.addSubview(label)
        return label
    }
    
    private func createSeparatingLineView() -> UIView {
        let separatingLine = UIView()
        separatingLine.translatesAutoresizingMaskIntoConstraints = false
        separatingLine.backgroundColor = .separatingLineColor
        contentView.addSubview(separatingLine)
        return separatingLine
    }
    //MARK:-SetupConstraint
    fileprivate let leftItemsOffset: CGFloat = 22.0
    fileprivate let rightItemsOffset: CGFloat = 20.0
    
    private func setupAllConstraintsToViews() {
        setupConstraintsToAdressTitleLabel()
        setupConstraintsToAdressDescriptionLabel()
        setupConstraintsToSeparatingLineView()
    }
    
    private func setupConstraintsToAdressTitleLabel() {
        adressTitleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: leftItemsOffset).isActive = true
        adressTitleLabel.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        adressTitleLabel.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -15).isActive = true
    }
    
    private func setupConstraintsToAdressDescriptionLabel() {
        adressDescriptionLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -rightItemsOffset).isActive = true
        adressDescriptionLabel.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        adressDescriptionLabel.leftAnchor.constraint(equalTo: adressTitleLabel.rightAnchor, constant: 5).isActive = true
        adressDescriptionLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5).isActive = true
    }
    
    private func setupConstraintsToSeparatingLineView() {
        separatingView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: leftItemsOffset).isActive = true
        separatingView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -rightItemsOffset).isActive = true
        separatingView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        separatingView.heightAnchor.constraint(equalToConstant: 1.0).isActive = true
    }
}
