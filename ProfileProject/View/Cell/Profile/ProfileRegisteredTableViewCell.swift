//
//  ProfileRegisteredTableViewCell.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/7/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import UIKit

class ProfileRegisteredTableViewCell: IdentifiableTableViewCell {
    
    lazy var registeredTitleLabel: UILabel = self.createLabel(with: UIFont.systemFont(ofSize: 16, weight: .regular), alignment: .left)
    lazy var registeredDescriptionLabel: UILabel = self.createLabel(with: UIFont.systemFont(ofSize: 16, weight: .medium), alignment: .right)
    fileprivate lazy var separatingView: UIView = self.createSeparatingLineView()
    
    //MARK:-Loading
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupAllConstraintsToViews()
        selectionStyle = .none
        if UIDevice.current.userInterfaceIdiom == .pad {
            registeredTitleLabel.font = UIFont.systemFont(ofSize: 26, weight: .regular)
            registeredDescriptionLabel.font = UIFont.systemFont(ofSize: 26, weight: .medium)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateModel(_ model: CellIdentifiable?, viewModel: ViewModelCellPresentable?) {
        guard let _ = viewModel as? ProfileViewModel else { return }
        guard let model = model as? ProfileRegisteredCellModel else { return }
        
        self.registeredTitleLabel.text = model.title
        self.registeredDescriptionLabel.text = Date().convertDateFormatter(date: model.date)
    }
    
    //MARK:-SetupViews
    private func createLabel(with font: UIFont, alignment: NSTextAlignment) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = alignment
        label.lineBreakMode = .byWordWrapping
        label.font = font
        label.textColor = .black
        label.numberOfLines = 0
        label.sizeToFit()
        contentView.addSubview(label)
        return label
    }
    
    private func createSeparatingLineView() -> UIView {
        let separatingLine = UIView()
        separatingLine.translatesAutoresizingMaskIntoConstraints = false
        separatingLine.backgroundColor = .separatingLineColor
        contentView.addSubview(separatingLine)
        return separatingLine
    }
    //MARK:-SetupConstraint
    fileprivate let leftItemsOffset: CGFloat = 22.0
    fileprivate let rightItemsOffset: CGFloat = 20.0
    
    private func setupAllConstraintsToViews() {
        setupConstraintsToRegisteredTitleLabel()
        setupConstraintsToRegisteredDescriptionLabel()
        setupConstraintsToSeparatingLineView()
    }
    
    private func setupConstraintsToRegisteredTitleLabel() {
        registeredTitleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: leftItemsOffset).isActive = true
        registeredTitleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        registeredTitleLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.5).isActive = true
    }
    
    private func setupConstraintsToRegisteredDescriptionLabel() {
        registeredDescriptionLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -rightItemsOffset).isActive = true
        registeredDescriptionLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        registeredDescriptionLabel.leftAnchor.constraint(equalTo: registeredTitleLabel.rightAnchor, constant: 10).isActive = true
        registeredDescriptionLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.5).isActive = true
    }
    
    private func setupConstraintsToSeparatingLineView() {
        separatingView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: leftItemsOffset).isActive = true
        separatingView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -rightItemsOffset).isActive = true
        separatingView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        separatingView.heightAnchor.constraint(equalToConstant: 1.0).isActive = true
    }
}
