//
//  ProgressHud.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/7/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import UIKit

protocol ProgressHudManager{
    func show()
    func hide()
}
class ProgressHudView: UIVisualEffectView, ProgressHudManager {
    
    private var text: String? {
        didSet {
            label.text = text
        }
    }
    private var activityIndicatorStyle: UIActivityIndicatorView.Style
    private var activityIndicatorColor: UIColor
    private var textColor: UIColor
    private let blurEffect: UIBlurEffect
    private lazy var activityIndictor: UIActivityIndicatorView = self.createActivityIndicatorView(withActivityIndicatorStyle: activityIndicatorStyle, color: activityIndicatorColor)
    private lazy var label: UILabel = self.createTextLabel(withTextColor: textColor)
    private lazy var vibrancyView: UIVisualEffectView = self.createVibrancyViewWith(effect: UIVibrancyEffect(blurEffect: self.blurEffect))
    
    //MAKR:-Loading
    init(text: String, indicatorStyle: UIActivityIndicatorView.Style, indicatorColor: UIColor, textColor: UIColor, blurEffectStyle: UIBlurEffect.Style) {
        self.text = text
        self.activityIndicatorStyle = indicatorStyle
        self.activityIndicatorColor = indicatorColor
        self.textColor = textColor
        self.blurEffect = UIBlurEffect(style: blurEffectStyle)
        super.init(effect: blurEffect)
        addAllConstraintsToViews()
        self.layer.cornerRadius = 8.0
        self.clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.text = ""
        self.activityIndicatorStyle = .gray
        self.textColor = .gray
        self.blurEffect = UIBlurEffect(style: .extraLight)
        self.activityIndicatorColor = .gray
        super.init(coder: aDecoder)
        addAllConstraintsToViews()
    }
    //MAKR:-SetupViews
    private func createActivityIndicatorView(withActivityIndicatorStyle style: UIActivityIndicatorView.Style, color: UIColor) -> UIActivityIndicatorView {
        let indicator = UIActivityIndicatorView(style: style)
        indicator.color = color
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.stopAnimating()
        indicator.hidesWhenStopped = true
        contentView.addSubview(indicator)
        return indicator
    }
    
    private func createTextLabel(withTextColor color: UIColor) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        label.textAlignment = .center
        label.textColor = color
        label.numberOfLines = 1
        label.sizeToFit()
        label.adjustsFontSizeToFitWidth = true
        label.text = self.text
        contentView.addSubview(label)
        return label
    }
    
    private func createVibrancyViewWith(effect: UIVibrancyEffect) -> UIVisualEffectView {
        let vibrancyView: UIVisualEffectView = UIVisualEffectView(effect: effect)
        vibrancyView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(vibrancyView)
        return vibrancyView
    }
    //MARK:-AddConstraints
    private func addAllConstraintsToViews() {
        addConstraintsToVibrancyView()
        addConstraintsToTextLabel()
        addConstraintsToActivityIndicatorView()
    }
    
    private func addConstraintsToVibrancyView() {
        vibrancyView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        vibrancyView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        vibrancyView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        vibrancyView.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
    }
    
    private func addConstraintsToTextLabel() {
        label.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        label.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        label.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        label.bottomAnchor.constraint(equalTo: activityIndictor.topAnchor, constant: -10).isActive = true
    }
    
    private func addConstraintsToActivityIndicatorView() {
        activityIndictor.topAnchor.constraint(equalTo: centerYAnchor, constant: 10).isActive = true
        activityIndictor.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        activityIndictor.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        activityIndictor.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
    }
    
    public func show() {
        self.isHidden = false
        activityIndictor.startAnimating()
    }
    
    public func hide() {
        self.isHidden = true
        activityIndictor.stopAnimating()
    }
}
