//
//  Configuration.swift
//  ProfileProject
//
//  Created by Vladymyr on 3/6/19.
//  Copyright © 2019 vsdacc.teach. All rights reserved.
//

import Foundation

struct Configuration {
    
    static var debug: Bool {
        return false
    }
    
    static var production: Bool {
        return true
    }
    
    static var testEndpoint: URL {
        return URL(string: "")!
    }
    
    static var productionEndpoint: URL {
        return URL(string: "")!
    }
    
    static var encriptionKey: String{
        return ""
    }
    
    static var requiredEndpoint: URL {
        return production ? productionEndpoint : testEndpoint
    }
    
    static var profileEndpoint: URL {
        return URL(string: "https://api.randomuser.me/")!
    }
    
    static var login: String {
        return "iosTestAssignment"
    }
    
    static var password: String {
        return "i0S.Test.Assignment"
    }
}
