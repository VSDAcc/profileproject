//
//  DispatchQueue.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 11/21/18.
//  Copyright © 2018 VSDAcc. All rights reserved.
//

import Foundation

extension DispatchQueue {
    
    static let networking = DispatchQueue(label: "com.project.name.networking",
                                          qos: .default,
                                          attributes: .concurrent,
                                          autoreleaseFrequency: .inherit,
                                          target: nil)
    
    static let background = DispatchQueue(label: "com.project.name.background",
                                          qos: .background,
                                          attributes: .concurrent,
                                          autoreleaseFrequency: .inherit,
                                          target: nil)
    
    static let monitoring = DispatchQueue(label: "com.project.name.monitoring",
                                          qos: .utility,
                                          attributes: .concurrent,
                                          autoreleaseFrequency: .inherit,
                                          target: nil)
}
