//
//  Image.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 3/4/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImage {
    var decompressedImage: UIImage {
        UIGraphicsBeginImageContextWithOptions(size, true, 0)
        draw(at: CGPoint.zero)
        guard let decompressedImage = UIGraphicsGetImageFromCurrentImageContext() else {
            return UIImage()
        }
        UIGraphicsEndImageContext()
        return decompressedImage
    }
}
extension UIImageView {
    func downloadImageUsingCache(stringURL: String) {
        DispatchQueue.global(qos: .userInteractive).async {
            let imageManager = SDWebImageManager.shared()
            if let image = imageManager.imageCache?.imageFromCache(forKey: stringURL) {
                DispatchQueue.main.async {
                    self.image = image
                }
                return
            } else {
                if let imageURL = URL(string: stringURL) {
                    _ = imageManager.imageDownloader?.downloadImage(with: imageURL, options: [.continueInBackground,.progressiveDownload], progress: nil, completed: {  (image, data, error, completed) in
                        if completed {
                            DispatchQueue.main.async {
                                self.image = image
                            }
                            imageManager.imageCache?.store(image, forKey: stringURL, completion: nil)
                        }
                    })
                }
            }
        }
    }
}
