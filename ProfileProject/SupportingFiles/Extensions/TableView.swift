//
//  TableView.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 3/3/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import UIKit
extension UITableView {
    func isLastVisibleCell(at indexPath: IndexPath) -> Bool {
        guard let lastIndexPath = indexPathsForVisibleRows?.last else {
            return false
        }
        
        return lastIndexPath == indexPath
    }
}
