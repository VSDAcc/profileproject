//
//  Color.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 2/27/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, alpha: CGFloat) {
        self.init(red: r / 255, green: g / 255, blue: b / 255, alpha: alpha)
    }
    
    static func random() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
    
    class var aliceBlue: UIColor {
        return UIColor(r: 248, g: 248, b: 255, alpha: 1)
    }
    
    class var loadingGrayColor: UIColor {
        return UIColor(r: 138, g: 153, b: 161, alpha: 1)
    }
    
    class var statusBarColor: UIColor {
        return UIColor(r: 41, g: 49, b: 52, alpha: 1)
    }
    
    class var separatingLineColor: UIColor {
        return UIColor(r: 138, g: 153, b: 161, alpha: 0.2)
    }
}
