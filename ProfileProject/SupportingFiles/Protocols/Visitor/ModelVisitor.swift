//
//  ModelVisitor.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 2/27/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import Foundation

protocol ModelVisitor {
    associatedtype T
    func visit(model: ProfilePostalAdressCellModel) -> T
}

protocol VisitableModel where Self: BaseCellModel {
    func accept<V: ModelVisitor>(visitor: V) -> V.T
}
