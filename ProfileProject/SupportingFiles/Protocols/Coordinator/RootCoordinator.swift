//
//  RootCoordinator.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 3/6/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import UIKit
import Foundation

protocol RootCoordinator: class {
    var childCoordinators: [Coordinator] {get set}
    func removeCoordinator(coordinator: Coordinator)
}
extension RootCoordinator {
    
    func removeCoordinator(coordinator: Coordinator) {
        
        var idx:Int?
        for (index, value) in childCoordinators.enumerated() {
            if value === coordinator {
                idx = index
                break
            }
        }
        
        if let index = idx {
            childCoordinators.remove(at: index)
        }
    }
}
protocol Coordinator: class {
    func start()
}
