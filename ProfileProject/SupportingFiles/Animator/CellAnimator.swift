//
//  CellAnimator.swift
//  UranC.TestApp
//
//  Created by Vladymyr on 3/3/19.
//  Copyright © 2019 VSDAcc. All rights reserved.
//

import UIKit

final class CellAnimator {
    private var hasAnimatedAllCells = false
    private let animation: CellAnimation
    
    init(animation: @escaping CellAnimation) {
        self.animation = animation
    }
    
    func animate(cell: UITableViewCell, at indexPath: IndexPath, in tableView: UITableView) {
        guard !hasAnimatedAllCells else {
            return
        }
        animation(cell, indexPath, tableView)
        hasAnimatedAllCells = tableView.isLastVisibleCell(at: indexPath)
    }
    
    func animate(cell: UICollectionViewCell, at indexPath: IndexPath, in collectionView: UICollectionView) {
        guard !hasAnimatedAllCells else {
            return
        }
        animation(cell, indexPath, collectionView)
        hasAnimatedAllCells = collectionView.isLastVisibleCell(at: indexPath)
    }
}
